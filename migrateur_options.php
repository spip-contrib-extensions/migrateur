<?php

/**
 * Options du migrateur
 *
 * @package SPIP\Migrateur\Options
**/

if (!defined("_ECRIRE_INC_VERSION")) return;

if (version_compare(PHP_VERSION, '5.4.0', '<')) {
	include_spip('migrateur_compat_php_530');
}

/**
 * Autoload du répertoire class/
 *
 * @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader-examples.md
 * 
 * @param string $class The fully-qualified class name.
 * @return void
 */
spl_autoload_register(function ($class) {

    // project-specific namespace prefix
    $prefix = 'SPIP\\Migrateur\\';

    // base directory for the namespace prefix
    $base_dir = __DIR__ . '/class/';

    // does the class use the namespace prefix?
    $len = strlen($prefix);

    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }

    // get the relative class name
    $relative_class = substr($class, $len);

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
});
